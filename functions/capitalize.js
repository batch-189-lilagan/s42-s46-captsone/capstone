const userController = require('../controllers/userController.js')

module.exports = capitalize = (name) => {

    let tmpNameArr = name.toLowerCase().split(' ');

    for (i = 0; i < tmpNameArr.length; i++) {
        tmpNameArr[i] = tmpNameArr[i][0].toUpperCase() + tmpNameArr[i].slice(1).toLowerCase();
    }

    return tmpNameArr.join(' ');

};