const userController = require('../controllers/userController.js')

module.exports = validateName = (firstName, lastName) => {

    if (firstName.length < 3 || lastName.length < 3) {
        return false;
    }

    else {
        let tmpFullName = `${firstName} ${lastName}`;
        let numberRegex = /\d+/;
    
        if (tmpFullName.match(numberRegex)) {
            return false;
        }
    
        else {
            return true;
        }
    }
};