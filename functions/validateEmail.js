const userController = require('../controllers/userController.js')

module.exports = validateEmail = (email) => {

    let validRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (email.match(validRegex)) {
        return true;
    }
    
    return false;

};