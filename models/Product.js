const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

    name: {
        type: String,
        required: [true, 'Product Name is required']
    },

    description: {
        type: String,
        required: [true, 'Product Description is required']
    },

    picture: {
        type: String,
        required: [true, 'Product picture is required']
    },

    price: {
        type: Number,
        required: [true, 'Product Price is required']
    },

    stocks: {
        type: Number,
        required: [true, 'Product stock is required']
    },

    isActive: {
        type: Boolean,
        default: true
    },

    createdOn: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model('Product', productSchema);