const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
        require: [true, 'User ID is required']
    },

    email: {
        type: String,
        required: [true, 'Email is required']
    },

    orderCart: [
        {
            productId: {
                type: String,
                required: [true, 'Product ID is required']
            },

            productName: {
                type: String,
                required: [true, 'Product name is required']
            },

            quantity: {
                type: Number,
                required: [true, 'Product quantity is required']
            },

            price: {
                type: Number,
                required: [true, 'Product Price is required']
            },

            subTotal: {
                type: Number,
                required: [true, 'Subtotal is required']
            }
        }
    ],

    total: {
        type: Number,
        default: 0
    },

    isDelivered: {
        type: Boolean,
        default: false
    },

    placedOn: {
        type: Date,
        default: new Date()
    }

});

module.exports = mongoose.model('Order', orderSchema);