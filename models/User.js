const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({

    firstName: {
        type: String,
        required: [true, 'First Name is required']
    },

    lastName: {
        type: String,
        required: [true, 'Last Name is requried']
    },

    email: {
        type: String,
        required: [true, 'Email is required']
    },

    password: {
        type: String,
        required: [true, 'Password is required']
    },

    isAdmin: {
        type: Boolean,
        default: false
    },

    userCart: [
        {
            productId: {
                type: String,
            },
            productName: {
                type: String
            },
            quantity: {
                type: Number,
            },
            price: {
                type: Number,
            },
            subTotal: {
                type: Number,
            },
            addedOn: {
                type: Date,
                default: new Date()
            }
        }
    ],

    registeredOn: {
        type: Date,
        default: new Date()
    }

});

module.exports = mongoose.model('User', userSchema);