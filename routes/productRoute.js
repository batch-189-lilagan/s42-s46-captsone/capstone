const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController.js');
const auth = require('../auth.js');

// Validation
const {validateProduct} = require('../middleware/validateProduct.js');
const {validateUpdateProduct} = require('../middleware/validateUpdateProduct.js');
const {isAdmin} = require('../middleware/isAdmin.js');

// [Admin: Creating a new product]
router.post('/add', auth.verify, isAdmin, validateProduct, productController.addProduct)

// [Admin: Update one product]
router.patch('/:productId/update', auth.verify, isAdmin, productController.updateOneProduct);

// Admin: Archive a product
router.patch('/:productId/archive', auth.verify, isAdmin, productController.archiveProduct);

// Admin: Enable a product
router.patch('/:productId/enable', auth.verify, isAdmin, productController.enableProduct);

// [User: Retrieve all active products]
router.get('/', productController.getActiveProducts);

// [Admin: Retrieve all products]
router.get('/all', auth.verify, isAdmin, productController.getProducts);

// [Admin / User: Retrieve one product based on product id]
router.get('/:productId', productController.getOneProduct);


module.exports = router;