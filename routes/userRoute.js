const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController.js');
const auth = require('../auth.js');

// Validation
const {validateRegistration} = require('../middleware/validateRegistration.js');
const {validateLogin} = require('../middleware/validateLogin.js');
const {isAdmin} = require('../middleware/isAdmin.js');
const {isNotAdmin} = require('../middleware/isNotAdmin.js');
const {validateOrder} = require('../middleware/validateOrder.js');


// [Register a User]
router.post("/register", validateRegistration, userController.registerUser);

// [Retrieve User details]
router.get("/profile", auth.verify, userController.getProfile);

// [Login]
router.post("/login", validateLogin, userController.loginUser);

// [User: Add to cart]
router.put("/addToCart", auth.verify, isNotAdmin, userController.addToCart);

// [User: My Cart]
router.get("/myCart", auth.verify, isNotAdmin, userController.getCart);

// [User: Create Order]
router.post("/checkout", auth.verify, isNotAdmin, userController.createOrder);

// [User: Retrive All Orders under his/her id]
router.get("/myOrders", auth.verify, isNotAdmin, userController.userGetOrders);

// [Admin: Set a user to admin]
router.put("/:userId/setAsAdmin", auth.verify, isAdmin, userController.setAsAdmin);

// [Admin: Deliver order]
router.put("/:orderId/deliver", auth.verify, isAdmin, userController.deliverOrder);

// [Admin: Retrieve All Orders]
router.get("/orders", auth.verify, isAdmin, userController.adminGetOrders);

// [Admin: Get all users]
router.get('/all', auth.verify, isAdmin, userController.getAllUsers);

module.exports = router;

