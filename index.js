const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const port = 4002;

// Enable dotenv
const dotenv = require('dotenv');
dotenv.config();

// Require routes here
const userRoute = require('./routes/userRoute.js');
const productRoute = require('./routes/productRoute.js')


const app = express();

// Middleware to allow access to our backend
app.use(express.json());
app.use(express.urlencoded({ extended:true }));
app.use(cors());

// app.use(<Routes here>)
app.use('/users', userRoute);
app.use('/products', productRoute);


// Connect to our Database
mongoose.connect(`mongodb+srv://charleslilagandb:Mongodb09289955950@charleslilagan.chlqocw.mongodb.net/S42-Capstone?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;
db.on('error', () => console.error.bind(console, 'error'));
db.once('open', () => console.log('Connected to MongoDb'));


// Listen to the port
app.listen(process.env.PORT || port, () => console.log(`API is now online on port ${process.env.PORT || port}`));