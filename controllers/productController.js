const Product = require('../models/Product.js')
const capitalize = require('../functions/capitalize.js');
const { trusted } = require('mongoose');


// [Admin: Creating a new product controller]
module.exports.addProduct = (request, response) => {
    let newProduct = new Product({
        name: request.body.name,
        description: request.body.description,
        picture: request.body.picture,
        price: request.body.price,
        stocks: request.body.stocks
    });

    return newProduct.save().then((product, error) => {
        if (error) {
            return response.send({
                error: 'serverError'
            });
        }

        return response.send(true);
    });
}
// [End of - Admin: Creating a new product controller]


// [User: Retrieve all active products in our database]
module.exports.getActiveProducts = (request, response) => {

    return Product.find({isActive: true}).then((result,error) => {
        if (error) {
            return response.send({
                error: 'serverError'
            });
        }

        if (result.length == 0) {
            return response.send(false)
        }

        return response.send(result);
    });
};
// [End of - User: Retrieve all products in our database]

// [Admin: Retrieve all active products in our database]
module.exports.getProducts = (request, response) => {

    return Product.find().then((result,error) => {
        if (error) {
            return response.send({
                error: 'serverError'
            });
        }

        if (result.length == 0) {
            return response.send(false)
        }

        return response.send(result);
    });
};
// [End of - Admin: Retrieve all products in our database]


// [Admin / User: Retrieve all one product based on produt id]
module.exports.getOneProduct = (request, response) => {

    return Product.findById(request.params.productId)
        .then((result, error) => {

            if (error) {
                return response.send({
                    error: 'serverError'
                });
            }

            // If no result...
            if (result == null) {
                return response.send(false);
            }

            return response.send(result);
        });
};
// [End of - Admin / User: Retrieve all one product based on produt id]


// [Admin: Update a product]
module.exports.updateOneProduct = (request, response) => {

    let updatedProduct = {
        name: request.body.name,
        description: request.body.description,
        picture: request.body.picture,
        price: request.body.price,
        stocks: request.body.stocks
    }

    return Product.findByIdAndUpdate(request.params.productId, updatedProduct).then((product, error) => {
        if (error) {
            return response.send({
                error: 'serverError'
            })
        }

        return response.send(true);
    })
};
// [End of - Admin: Update a product]


// Admin: Archive a product
module.exports.archiveProduct = async (request, response) => {
    
    return Product.findByIdAndUpdate(request.params.productId, {isActive: false}).then((product, error) => {
        if (error) {
            return response.send({
                error: 'serverError'
            })
        }

        return response.send(true);
    }) 
}; 

// Admin: Enable a product
module.exports.enableProduct = async (request, response) => {
    
    return Product.findByIdAndUpdate(request.params.productId, {isActive: true}).then((product, error) => {
        if (error) {
            return response.send({
                error: 'serverError'
            })
        }

        return response.send(true);
    }) 
}; 