const User = require('../models/User.js')
const Product = require('../models/Product.js')
const capitalize = require('../functions/capitalize.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');
const Order = require('../models/Order.js');
const { trusted } = require('mongoose');

// [Register a User Controller]
module.exports.registerUser = async (request, response) => {

    let newUser = new User({
        firstName: capitalize(request.body.firstName),
        lastName: capitalize(request.body.lastName),
        email: request.body.email.toLowerCase(),
        password: bcrypt.hashSync(request.body.password, 10)
    });


    return newUser.save().then((user, error) => {
        if (error) {
            response.send({
                error: 'serverError'
            });
        }

        response.send(true);
    });
}
// [End of Register a User Controller]

// [Retrieve User details]
module.exports.getProfile = (request, response) => {

    const userId = auth.decode(request.headers.authorization).id;

    return User.findById(userId).then((result, error) => {
        if (error) {
            return response.send({
                error: 'serverError'
            });
        }

        result.password = '';

        return response.send(result);
    });

}
// [End of Retrieve User details]


// [Log in]
module.exports.loginUser = async (request, response) => {
    return User.findOne({email: request.body.email}).then((user, error) => {
        if (error) {
            return response.send({
                error: 'serverError'
            });
        }

        else {
            return response.send({
                access: auth.createAccessToken(user)
            });
        }
    });
};
// [End of Log in]


// [Admin: Set a user to admin]
module.exports.setAsAdmin = async (request, response) => {
    const userId = request.params.userId
    // Check first if the id is already an admin
    const userToUpdate = await User.findOne({_id: userId})
        .then((result, error) => {
            if (error) {
                return response.send({
                    error: 'serverError'
                });
            }

            if (result == null) {
                return null;
            }
            return result
    });

    if (userToUpdate == null) {
        return response.send({
            error: 'uidNotFound'
        });
    }

    if (userToUpdate.isAdmin) {
        return response.send({
            error: 'uidAlreadyAdmin'
        });
    }

    // If the id is not an admin yet, proceed
    return User.findByIdAndUpdate(userId, {isAdmin: true}).then((result, error) => {
        // If there is an error upon updating
        if (error) {
            return response.send({
                error: 'serverError'
            })
        }

        // If not an admin, and no error, return success
        return response.send(true);
    });
}
// [End of - Admin: Set a user to Admin]


// [User: Add to cart]
module.exports.addToCart = async (request, response) => {
    const userId = auth.decode(request.headers.authorization).id;

    let subTotal = request.body.price * request.body.quantity;

    let data = {
        productId: request.body.productId,
        productName: request.body.productName,
        quantity: request.body.quantity,
        price: request.body.price,
        subTotal: subTotal
    };

    const isUserCartUpdated = await User.findById(userId).then(user => {
        user.userCart.push(data);

        return user.save().then((result, error) => {
            if (error) {
                return false;
            }

            return true;
        });
    });

    const isProductUpdated = await Product.findById(request.body.productId).then(product => {

        let dbProdStocks = product.stocks;
        let bodyProdQty = request.body.quantity;

        let isOutOfStock =
        (dbProdStocks - bodyProdQty) <= 0
        ? true : false;

        if (isOutOfStock) {
            product.isActive = false;
        }

        product.stocks = product.stocks - request.body.quantity;

        return product.save().then((result, error) => {
            if (error) {
                return false;
            }

            return true;
        });
    });

    if (!(isUserCartUpdated && isProductUpdated)) {
        return response.send(false);
    }

    return response.send(true);
}
// [End of - User: Add to Cart]

// [User: My Cart]
module.exports.getCart = (request, response) => {

    const userId = auth.decode(request.headers.authorization).id;

    return User.findById(userId).then((user, error) => {
        if (error) {
            return response.send({
                error: 'serverError'
            });
        }

        return response.send(user.userCart)
    })

}
// [End of - User: My Cart]


// [User: Check out]
module.exports.createOrder = async (request, response) => {

    const userId = auth.decode(request.headers.authorization).id;

    let email = auth.decode(request.headers.authorization).email;

    let userCart = await User.findById(userId).then((user, error) => {
        if (error) {
            return false;
        }

        if (user.userCart.length === 0) {
            return 'empty'
        }

        return user.userCart;
    });

    if (userCart === false) {
        return response.send({
            error: 'serverError'
        });
    }

    if (userCart === 'empty') {
        return response.send({
            error: 'emptyCart'
        });
    }

    let total = 0;
    
    for (i = 0; i < userCart.length; i++) {
        total += userCart[i].price;
    }

    let newOrder = new Order ({
        userId: userId,
        email: email,
        orderCart: userCart,
        total: total,
        purchasedOn: new Date()
    })

    let removeUserCart = await User.findById(userId).then((user, error) => {
        if (error) {
            return false;
        }

        user.userCart = []
        
        return user.save().then((result, error) => {
            if (error) {
                return false;
            }

            return true;
        });
    });

    if (removeUserCart === false) {
        return response.send({
            error: 'serverError'
        })
    }

    return newOrder.save().then((result, error) => {
        if (error) {
            response.send({
                error: 'serverError'
            })
        }
        return response.send(true);
    })

}
// [End of - User: Create an order]


// [Admin: Get all orders]
module.exports.adminGetOrders = (request, response) => {

    return Order.find().then((result, error) => {
        if (error) {
            return response.send({
                error: 'serverError'
            });
        }

        if (result.length == 0) {
            return response.send(false)
        }

        return response.send(result);
    })

}
// [End of - Admin: Get all orders]


// [User: My Orders]
module.exports.userGetOrders = (request, response) => {

    let userId = auth.decode(request.headers.authorization).id

    return Order.find({userId: userId}).then((result, error) => {
        if (error) {
            return response.send({
                error: 'serverError'
            });
        }

        if (result.length == 0) {
            return response.send(false);
        }

        return response.send(result);
    })

}
// [End of - User: My Orders]

// [Admin: Deliver Order]
module.exports.deliverOrder = (request, response) => {

    let orderId = request.params.orderId;

    return Order.findByIdAndUpdate(orderId, {isDelivered: true}).then((result, error) => {
        if (error) {
            return response.send({
                error: 'serverError'
            });
        }

        if (result === null) {
            return response.send(false)
        }

        return response.send(true);
    })

}
// [End of - Admin: Deliver Order]

// [Admin - Get all users]
module.exports.getAllUsers = (request, response) => {
    return User.find({}).then((result, error) => {
        if (error) {
            return response.send(false)
        }
        if (result.length === 0) {
            return response.send(false)
        }

        return response.send(result);
    })
}
// [End]