const Product = require('../models/Product.js');

module.exports.validateUpdateProduct = async (request, response, next) => {
    // If the request body is completely empty for this process
    if (!request.body.hasOwnProperty('name') && !request.body.hasOwnProperty("description") && !request.body.hasOwnProperty("price")) {
        return response.status(400).send({errors: "No request body found"});
    }
    

    let fieldsToUpdate = [];
    const errors = {invalid: []};

    for (let field in request.body) {
        fieldsToUpdate.push(field);
    }

    for (i = 0; i < fieldsToUpdate.length; i++) {
        if (request.body[fieldsToUpdate[i]].length == 0) {
            // if value in reqest body is empty
            errors.invalid.push(
                {
                    value: `${request.body[fieldsToUpdate[i]]}`,
                    msg: `${fieldsToUpdate[i]} must not be empty`,
                    param: fieldsToUpdate[i],
                    location: "body"
                }
            );
        }
    }

    if (errors.invalid.length > 0) {
        return response.status(400).send(errors);
    }

    // If no errors in the request body, check the existence of the product id
    const productExists = await Product.findOne({_id: request.params.productId})
        .then((result, error) => {

            if (error) {
                return response.status(500).send({errors: error});
            }

            if (result) {
                return true;
            }

            return false
        });

    // If the product id doesn't exist...
    if (!productExists) {
        return response.status(400).send({failed: `Product doesn't exist`});
    }

    // If the product exists, and no errors in the validation, proceed...
    next();
}