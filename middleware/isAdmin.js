const auth = require('../auth.js');

module.exports.isAdmin = (request, response, next) => {
    const isAdmin = auth.decode(request.headers.authorization).isAdmin;

    if (!isAdmin) {
        return response.send({
            error: 'notAnAdmin'
        })
    }
    next();
};