const { check, validationResult } = require('express-validator');
const User = require('../models/User.js')

module.exports.validateRegistration = [
    check("firstName")
        .trim()
        .isLength({min:3}).withMessage("First name must be at least 3 characters")
        .isAlpha('en-US', {ignore: ' '}).withMessage("First name must have alpha characters only"),

    check("lastName")
        .trim()
        .isLength({min:3}).withMessage("First name must be at least 3 characters")
        .isAlpha('en-US', {ignore: ' '}).withMessage("First name must have alpha characters only"),

    check("email", "Email must be a valid email address")
        .isEmail()
        .trim()
        .escape(),
    
    check("password", "Password must be at least 4 characters")
        .isLength({min:4})
        .trim()
        .escape(),

    (request, response, next) => {
        const errors = validationResult(request);

        // If there are errors in the validation
        if (!errors.isEmpty()) {
            return response.send({
                error: 'invalidInputs'
            })
        }

        // Check if email exists
        return User.findOne({email: request.body.email}).then((result, error) => {
            if (error) {
                return response.send({
                    error: 'serverError'
                });
            }

            if (result) {
                return response.send({
                    error: 'emailExists'
                });
            }

            // If email does not exist, proceed with the controller
            next();
        })
        
    },
];