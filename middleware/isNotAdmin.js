const auth = require('../auth.js');

module.exports.isNotAdmin = (request, response, next) => {
    const isAdmin = auth.decode(request.headers.authorization).isAdmin;

    if (isAdmin) {
        return response.send({
            error: 'admin'
        })
    }
    next();
};