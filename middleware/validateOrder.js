const { check, validationResult } = require('express-validator');
const Product = require('../models/Product.js');

module.exports.validateOrder = [
    check("products")
        .notEmpty().withMessage("No product/s to add in order"),

    async (request, response, next) => {
        const errors = validationResult(request);

        // If there are errors in the validation
        if (!errors.isEmpty()) {
            return response.send({
                error: 'invalidInputs'
            });
        }

        next();


        // const productIds = request.body.products;
        // const products = await Product.find().where('_id').in(productIds).exec();

        // // (1) If one of the products is non-existing...
        // if (productIds.length !== products.length) {
        //     return response.status(400).send({invalid: 'Products input may have a non-existing product in our database'});
        // }

        // // (2) Check if the products found in our db has an archived product
        // for (i = 0; i < products.length; i++) {
        //     if (products[i].isActive == false) {
        //         return response.status(400).send({invalid: "Products input may have an archived product. Will not proceed to creation of order."})
        //     }
        // }

        // // (3) Check if all the products in the request body doesn't exist in our db
        // if (products.length == 0) {
        //     return response.status(400).send({invalid: "No products found"});
        // }

        // next();
    }
];