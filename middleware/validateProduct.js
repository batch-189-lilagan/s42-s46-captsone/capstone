const { check, validationResult } = require('express-validator');
const Product = require('../models/Product.js');

module.exports.validateProduct = [
    check("name")
        .trim()
        .notEmpty().withMessage('Product name must not be empty.'),

    check("description")
        .trim()
        .notEmpty().withMessage('Product description must not be empty'),
        
    check("price")
        .notEmpty().withMessage('Price must not be empty'),

    async (request, response, next) => {
        const errors = validationResult(request);

        // If there are errors in the validation
        if (!errors.isEmpty()) {
            return response.send({
                error: 'invalidInputs'
            });
        }

        // Check if the product already exists in our database
        const productExists = await Product.findOne({name: capitalize(request.body.name)})
            .then((result, error) => {
                if (error) {
                    return response.send({
                        error: 'serverError'
                    });
                }
                if (result) {
                    return true
                }

                return false
            });

        if (productExists) {
            return response.send({
                error: 'productExists'
            });
        }

        // If request body passess our validation and doesn't exist in our database
        next();
    }
]