const { check, validationResult } = require('express-validator');
const User = require('../models/User.js');
const bcrypt = require('bcrypt');

module.exports.validateLogin = [
    check("email", "")
        .notEmpty().withMessage("Please enter an email")
        .isEmail().withMessage("Please enter a valid Email address")
        .trim()
        .escape(),

    check("password")
        .notEmpty().withMessage("Please enter a password")
        .trim()
        .escape(),

    (request, response, next) => {
        const errors = validationResult(request);
        // If there are errors in the validation
        if (!errors.isEmpty()) {
            return response.send({
                error: 'invalidInputs'
            });
        }

        // Check if the email already exists in our database
        return User.findOne({email: request.body.email}).then((result, error) => {

            if (error) {
                return response.send({
                    error: 'serverError'
                });
            }

            // If request body email doesn't exist in our database
            if (result == null) {
                return response.send({
                    error: 'emailNotFound'
                });
            }

            // Check if the passsword for the email existing in our database matches the password in request body
            const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

            if (!isPasswordCorrect) {
                return response.send({
                    error: 'incPassword'
                });
            }

            // If all the inputs are valid proceed with the controller
            next();
        })
    }
];